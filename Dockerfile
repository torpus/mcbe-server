FROM ubuntu:latest
ARG VERSION
ENV INSTALL_DIR=/mcbe LD_LIBRARY_PATH=.
LABEL maintainer "Torpus"

RUN apt-get update && \
    apt-get install libcurl4-openssl-dev unzip -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR $INSTALL_DIR

ADD https://minecraft.azureedge.net/bin-linux/bedrock-server-$VERSION.zip .

RUN unzip bedrock-server-$VERSION.zip \
    && rm bedrock-server-$VERSION.zip \
    && chmod +x $INSTALL_DIR/bedrock_server

COPY server.properties $INSTALL_DIR/server.properties

EXPOSE 19132/tcp 19132/udp 19133/tcp 19133/udp

CMD "$INSTALL_DIR/bedrock_server"
