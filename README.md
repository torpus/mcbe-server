# Minecraft Bedrock Edition Server

This project wraps the Minecraft Bedrock Server in a docker container for easily running locally.
It allows for local multiplayer of Windows and Xbox clients.  Switch is not working at this time.

## Configuration

Update the `server.properties` file with your preferences.  It will be loaded into the container over the default that comes with the server download if you do a `docker build .`.

## Usage

`docker run -d --restart unless-stopped -p 19132:19132/tcp -p 19132:19132/udp -p 19133:19133/tcp -p 19133:19133/udp -v /localpath/mcbe-server/worlds:/mcbe/worlds --name <optional-name> registry.gitlab.com/torpus/mcbe-server:latest`